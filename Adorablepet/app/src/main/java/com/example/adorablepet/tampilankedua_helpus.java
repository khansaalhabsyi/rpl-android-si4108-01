package com.example.adorablepet;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;


import java.util.HashMap;
import java.util.Map;

public class tampilankedua_helpus extends AppCompatActivity {

    private static final String TAG = tampilankedua_helpus.class.getSimpleName();
    EditText editFName,editAmount,editdeskripsi;
    String data;
    DatabaseReference reff;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.tampilankedua_helpus);

        editFName = findViewById(R.id.editText11);
        editAmount = findViewById(R.id.editText4);
        editdeskripsi=findViewById(R.id.editText12);


    }

    public void update(View view) {


        String fname = editFName.getText().toString();
        String amount = editAmount.getText().toString();
        String deskripsi = editdeskripsi.getText().toString();

        if (TextUtils.isEmpty(fname)) {

            Toast.makeText(this, "First name tidak boleh kosong", Toast.LENGTH_SHORT).show();


        } else {


            if (TextUtils.isEmpty((amount))) {

                DatabaseReference ref = FirebaseDatabase.getInstance().getReference().child("nilai").child(fname);
                Map<String, Object> updates = new HashMap<String, Object>();

                updates.put("Deskripsi", deskripsi);

                ref.updateChildren(updates);
                Toast.makeText(this,"Data berhasil di update",Toast.LENGTH_SHORT).show();

            } else if (TextUtils.isEmpty((deskripsi))) {

                DatabaseReference ref = FirebaseDatabase.getInstance().getReference().child("nilai").child(fname);
                Map<String, Object> updates = new HashMap<String, Object>();

                updates.put("Amount", amount);

                ref.updateChildren(updates);
                Toast.makeText(this,"Data berhasil di update",Toast.LENGTH_SHORT).show();

            } else {
                DatabaseReference ref = FirebaseDatabase.getInstance().getReference().child("nilai").child(fname);
                Map<String, Object> updates = new HashMap<String, Object>();

                updates.put("Amount", amount);
                updates.put("Deskripsi", deskripsi);


                ref.updateChildren(updates);

                Toast.makeText(this,"Data berhasil di update",Toast.LENGTH_SHORT).show();
            }

        }
    }

    public void helpus2(View view){

        Intent intent = new Intent (this, tampilanakhir_donation.class );
        startActivity(intent);
    }

    public void config (View view){


    }
}

