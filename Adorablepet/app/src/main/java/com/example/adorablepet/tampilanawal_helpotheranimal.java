package com.example.adorablepet;

import android.app.ProgressDialog;
import android.content.Intent;

import android.net.Uri;
import android.os.Bundle;

import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;



import androidx.appcompat.app.AppCompatActivity;


import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import android.content.ContentResolver;
import android.graphics.Bitmap;
import android.provider.MediaStore;
import android.webkit.MimeTypeMap;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.storage.OnProgressListener;
import com.google.firebase.storage.UploadTask;
import java.io.IOException;





public class tampilanawal_helpotheranimal extends AppCompatActivity {

    private static final String TAG = tampilanawal_helpotheranimal.class.getSimpleName();
    EditText editFName,editLName,editAmount,editAge,editRekening,editdeskripsi,editId,txtdata;
    String data;
    DatabaseReference reff;
    StorageReference mStorageRef;
    FirebaseDatabase database;
    DatabaseReference Myref;
    FirebaseStorage storage;
    nilai nilai;
    Button btnupload,btnsubmit,btninput,btnnext ;
    Uri FilePathUri;
    DatabaseReference db;

    ImageView imageView;
    int Image_Request_Code = 7;
    ProgressDialog progressDialog ;
    public Uri download;
    FirebaseUser user;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.tampilanawal_helpotheranimal);


        editFName = findViewById(R.id.editText2);
        editLName = findViewById(R.id.editText3);
        editAmount = findViewById(R.id.editText4);
        editAge = findViewById(R.id.editText8);
        editRekening = findViewById(R.id.editText9);
        editdeskripsi = findViewById(R.id.editText5);
        editId = findViewById(R.id.editText13);
        btnsubmit=findViewById(R.id.button4);
        btnupload=findViewById(R.id.button3);
        btninput=findViewById(R.id.button11);
        txtdata=findViewById(R.id.editText14);
        btnnext=findViewById(R.id.button10);

        imageView=(ImageView) findViewById(R.id.imageView26);
        progressDialog = new ProgressDialog(tampilanawal_helpotheranimal.this);

        btnupload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent();
                intent.setType("image/*");
                intent.setAction(Intent.ACTION_GET_CONTENT);
                startActivityForResult(Intent.createChooser(intent, "Select Image"), Image_Request_Code);

            }
        });


        btninput.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                mStorageRef = FirebaseStorage.getInstance().getReference("Images");
                Myref = FirebaseDatabase.getInstance().getReference("Images");

                input();

            }
        });

        btnsubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {



                FirebaseDatabase database = FirebaseDatabase.getInstance();
                DatabaseReference Myref = database.getReference().child("nilai");


                String fname = editFName.getText().toString();
                String lname = editLName.getText().toString();
                String amount = editAmount.getText().toString();
                String age = editAge.getText().toString();
                String rekening = editRekening.getText().toString();
                String deskripsi = editdeskripsi.getText().toString();
                String ids = editId.getText().toString();




                if (TextUtils.isEmpty(fname) || TextUtils.isEmpty(lname) || TextUtils.isEmpty(age)) {


                    Toast.makeText(tampilanawal_helpotheranimal.this, "Nama / Surname / Marks tidak boleh kosong", Toast.LENGTH_SHORT).show();

                }else if (!TextUtils.isEmpty(ids)) {

                    nilai nilai = new nilai(Integer.valueOf(age),Integer.valueOf(ids),fname,lname,amount,rekening,deskripsi);
                    Myref.child(ids).setValue(nilai);

                    Toast.makeText(tampilanawal_helpotheranimal.this,"Data berhasil ditambahkan",Toast.LENGTH_SHORT).show();

                }


            }
        });

btnnext.setOnClickListener(new View.OnClickListener() {
    @Override
    public void onClick(View view) {




    }
});


    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == Image_Request_Code && resultCode == RESULT_OK && data != null && data.getData() != null) {

            FilePathUri = data.getData();

            try {
                Bitmap bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), FilePathUri);
                imageView.setImageBitmap(bitmap);
            }
            catch (IOException e) {

                e.printStackTrace();
            }
        }
    }

    public String GetFileExtension(Uri uri) {

        ContentResolver contentResolver = getContentResolver();
        MimeTypeMap mimeTypeMap = MimeTypeMap.getSingleton();
        return mimeTypeMap.getExtensionFromMimeType(contentResolver.getType(uri)) ;

    }



    public void helpOther(View view) {



    }

    public void next (View view){

        Intent intent = new Intent(this, tampilankedua_helpotheranimal.class);
        startActivity(intent);
    }

    public void input(){

        if (FilePathUri != null) {

            progressDialog.setTitle("Image is Uploading...");
            progressDialog.show();
            StorageReference storageReference2 = mStorageRef.child(System.currentTimeMillis() + "." + GetFileExtension(FilePathUri));
            storageReference2.putFile(FilePathUri)
                    .addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                        @Override
                        public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {

                            String TempImageName = txtdata.getText().toString().trim();
                            progressDialog.dismiss();
                            Toast.makeText(getApplicationContext(), "Image Uploaded Successfully ", Toast.LENGTH_LONG).show();
                            @SuppressWarnings("VisibleForTests")
                            uploadinfo imageUploadInfo = new uploadinfo(TempImageName, taskSnapshot.getUploadSessionUri().toString());
                            String ImageUploadId = Myref.push().getKey();
                            Myref.child(ImageUploadId).setValue(imageUploadInfo);
                        }
                    });
        }
        else {

            Toast.makeText(tampilanawal_helpotheranimal.this, "Please Select Image or Add Image Name", Toast.LENGTH_LONG).show();

        }
    }





    public void config (View view){
//Nyambung ke Ines

    }
}
