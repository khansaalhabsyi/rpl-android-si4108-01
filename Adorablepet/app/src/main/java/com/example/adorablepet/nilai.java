package com.example.adorablepet;

public class nilai {
    private String fname, lname, amount, rekening, deskripsi;
    private int age;
    private int id;


    public nilai(Integer age,Integer id, String fname, String lname, String amount, String rekening, String deskripsi) {
        this.fname = fname;
        this.lname = lname;
        this.amount = amount;
        this.rekening = rekening;
        this.deskripsi = deskripsi;
        this.age = age;
        this.id = id;

    }

    public String getFname() {
        return fname;
    }

    public void setFname(String fname) {
        this.fname = fname;
    }

    public String getLname() {
        return lname;
    }

    public void setLname(String lname) {
        this.lname = lname;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getRekening() {
        return rekening;
    }

    public void setRekening(String rekening) {
        this.rekening = rekening;
    }

    public String getDeskripsi() {
        return deskripsi;
    }

    public void setDeskripsi(String deskripsi) {
        this.deskripsi = deskripsi;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }


}
