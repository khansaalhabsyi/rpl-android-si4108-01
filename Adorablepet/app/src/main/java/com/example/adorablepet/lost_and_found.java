package com.example.adorablepet;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import androidx.appcompat.app.AppCompatActivity;

public class lost_and_found extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.lost_and_found);
    }


    public void inputpet(View view) {
        Intent intent = new Intent(this, lost_and_found4.class);
        startActivity(intent);
    }

    public void config(View view) {
        Intent intent = new Intent(this, config.class);
        startActivity(intent);
    }

    public void animaldesc(View view) {
        Intent intent = new Intent(this, lost_and_found1.class);
        startActivity(intent);
    }
    public void clickhome (View view) {

        startActivity(new Intent(this, lost_and_found4.class));
    }

    public void search(View view) {
        Intent intent = new Intent(this, lost_and_found3.class);
        startActivity(intent);
    }

    public void petidentity(View view) {
        Intent intent = new Intent(this, lost_and_found4.class);
        startActivity(intent);
    }
}
