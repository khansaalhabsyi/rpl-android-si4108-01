package com.example.adorablepet;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

public class tampilankedua_helpotheranimal extends AppCompatActivity {


    private static final String TAG = tampilankedua_helpotheranimal.class.getSimpleName();
    EditText editFName,editLName,editAmount,editAge,editRekening,editdeskripsi;
    String data;
    DatabaseReference reff;
    Button btnview;
    ArrayList<String> nilai = new ArrayList<>();
    FirebaseDatabase fb;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.tampilankedua_helpotheranimal);

        editFName = findViewById(R.id.editText2);
        editLName =findViewById(R.id.editText3);
        editAmount = findViewById(R.id.editText4);
        editAge =findViewById(R.id.editText8);
        editRekening=findViewById(R.id.editText9);
        editdeskripsi=findViewById(R.id.editText5);
        btnview=findViewById(R.id.button6);

        btnview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                data="";
                final FirebaseDatabase database = FirebaseDatabase.getInstance();
                DatabaseReference myRef = database.getReference().child("nilai");

                myRef.addValueEventListener(new ValueEventListener(){

                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {

                        ArrayList<nilai> notes = new ArrayList<nilai>();

                        for (DataSnapshot postSnapshot : dataSnapshot.getChildren()) {

                            nilai post = postSnapshot.getValue(nilai.class);
                            notes.add(post);


                        }


                        showMessage("Semua data", data);
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                        System.out.println("The read failed: " + databaseError.getCode());

                    }
                });

            }
        });

    }

    public void view(View view){



    }

    public void showMessage(String title, String Message){
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setCancelable(true);
        builder.setTitle(title);
        builder.setMessage(Message);
        builder.show();
    }

    public void helpUs (View view){
        Intent intent = new Intent (this, tampilankedua_helpus.class );
        startActivity(intent);

    }


    public void config (View view){


    }
}



