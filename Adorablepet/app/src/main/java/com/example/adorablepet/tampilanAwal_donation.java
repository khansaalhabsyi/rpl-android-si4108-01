package com.example.adorablepet;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import androidx.appcompat.app.AppCompatActivity;

public class tampilanAwal_donation extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.tampilanawal_donation);
    }

    public void helpButton(View view){

      Intent intent = new Intent (this, tampilanawal_helpotheranimal.class );
      startActivity(intent);
    }

    public void opendonation (View view){

        Intent intent = new Intent (this, tampilanawal_opendonation.class );
        startActivity(intent);
    }

    public void config (View view){


    }
}
