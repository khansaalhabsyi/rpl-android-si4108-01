package com.example.adorablepet.model;

public class Produk {
    private int id;
    private String urlToko;
    public String linkFoto;
    public String nama;
    public String usageFor;
    public String description;

    public Produk() {
        //NOT IMPLEMENTED
    }

    public int getId() {
        return id;
    }

    public String getUrlToko() {
        return urlToko;
    }
}
