package com.example.adorablepet;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import androidx.appcompat.app.AppCompatActivity;

public class lost_and_found5 extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.lost_and_found5);
    }


    public void inform(View view) {
        Intent intent = new Intent(this, lost_and_found.class);
        startActivity(intent);
    }
}