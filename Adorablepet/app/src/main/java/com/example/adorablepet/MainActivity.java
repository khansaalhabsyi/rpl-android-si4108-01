package com.example.adorablepet;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

public class MainActivity extends AppCompatActivity {

    ImageView imageView58;
    String goolgeMap = "com.google.android.apps.maps"; // identitas package aplikasi google masps android
    Uri gmmIntentUri;
    Intent mapIntent;
    String shelter_jakarta = "-6.277113, 106.825571";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        imageView58    = (ImageView) findViewById(R.id.imageView58);


        imageView58.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // Buat Uri dari intent string. Gunakan hasilnya untuk membuat Intent.
                gmmIntentUri = Uri.parse("google.navigation:q=" + shelter_jakarta);

                // Buat Uri dari intent gmmIntentUri. Set action => ACTION_VIEW
                mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);

                // Set package Google Maps untuk tujuan aplikasi yang di Intent yaitu google maps
                mapIntent.setPackage(goolgeMap);

                if (mapIntent.resolveActivity(getPackageManager()) != null) {
                    startActivity(mapIntent);
                } else {
                    Toast.makeText(MainActivity.this, "Google Maps Belum Terinstal. Install Terlebih dahulu.",
                            Toast.LENGTH_LONG).show();
                }
            }

        });

    }





    public void ButtonForDonate(View view){

        Intent intent = new Intent (this, tampilanAwal_donation.class );
        startActivity(intent);
    }

    public void ButtonForLostFound(View view){
        Intent intent = new Intent (this, lost_and_found.class );
        startActivity(intent);

    }

    public void ButtonForAdopt(View view){


    }

    public void ButtonForAnimalCare(View view){
        Intent intent = new Intent (this, com.example.adorablepet.contoller.MainActivity.class );
        startActivity(intent);

    }

    public void ButtonForShelter(View view){


    }

    public void config (View view){


    }


}

