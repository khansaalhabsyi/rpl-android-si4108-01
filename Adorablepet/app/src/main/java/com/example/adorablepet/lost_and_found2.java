package com.example.adorablepet;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import androidx.appcompat.app.AppCompatActivity;

public class lost_and_found2 extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.lost_and_found2);
    }

    public void backhome(View view) {
        Intent intent = new Intent(this, lost_and_found.class);
        startActivity(intent);
    }

    public void tohomex(View view) {
        Intent intent = new Intent(this, lost_and_found.class);
        startActivity(intent);

    }
}
