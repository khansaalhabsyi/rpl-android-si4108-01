package com.example.adorablepet;


import androidx.test.ext.junit.runners.AndroidJUnit4;
import androidx.test.rule.ActivityTestRule;


import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static androidx.test.espresso.Espresso.onView;
import static androidx.test.espresso.action.ViewActions.closeSoftKeyboard;
import static androidx.test.espresso.action.ViewActions.typeText;
import static androidx.test.espresso.matcher.ViewMatchers.withId;

public class Pendaftaranactivity {

@Rule
public ActivityTestRule<tampilanawal_helpotheranimal> mActivity = new ActivityTestRule<>(tampilanawal_helpotheranimal.class);

    private String image_name = "Hewan";
    private String Id = "2";
    private String Fname = "Lion";
    private String Lname = "nda";
    private String Amount = "cukup";
    private String Age = "3";
    private String Rekening = "tujuh";
    private String Deskripsi = "keren";

    @Test
    public void image_nameEditable() {
        onView(withId(R.id.editText14)).perform(typeText(image_name), closeSoftKeyboard());
    }

    @Test
    public void IdEditable() {
        onView(withId(R.id.editText13)).perform(typeText(Id), closeSoftKeyboard());
    }

    @Test
    public void FnameEditable() {
        onView(withId(R.id.editText2)).perform(typeText(Fname), closeSoftKeyboard());
    }

    @Test
    public void LnameEditable() {
        onView(withId(R.id.editText3)).perform(typeText(Lname), closeSoftKeyboard());
    }

    @Test
    public void AmountEditable() {
        onView(withId(R.id.editText4)).perform(typeText(Amount), closeSoftKeyboard());
    }

    @Test
    public void AgeEditable() {
        onView(withId(R.id.editText8)).perform(typeText(Age), closeSoftKeyboard());
    }

    @Test
    public void RekeningEditable() {
        onView(withId(R.id.editText9)).perform(typeText(Rekening), closeSoftKeyboard());
    }

    @Test
    public void DeskripsiEditable() {
        onView(withId(R.id.editText5)).perform(typeText(Deskripsi), closeSoftKeyboard());
    }




}
